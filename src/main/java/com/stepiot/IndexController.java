package com.stepiot;

import java.util.List;
import java.util.Map;
import java.util.Random;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {

  @Autowired JdbcTemplate jdbcTemplate;

  static final StringBuilder sb = new StringBuilder();

  static {
    sb.append(" SELECT * FROM bc_posts");
    sb.append(" ORDER BY create_time DESC ");
    sb.append(" LIMIT 4; ");
  }

  @GetMapping(value = {"/", "/index.html"})
  public String greeting(Model model) {
    List<Map<String, Object>> posts = jdbcTemplate.queryForList(sb.toString());
    model.addAttribute("data", posts);
    return "index";
  }

  private static final Random random = new Random();

  @GetMapping("/about.html")
  public String about(Model model) {
    return "about";
  }

  @GetMapping("/case.html")
  public String cases(Model model) {
    return "case";
  }

  @GetMapping("/classroom.html")
  public String classroom(Model model) {
    return "classroom";
  }

  @GetMapping("/contact.html")
  public String contact(Model model) {
    return "contact";
  }

  @GetMapping("/hive.html")
  public String hive(Model model) {
    return "hive";
  }

  @GetMapping("/info.html")
  public String info(Model model) {
    return "info";
  }

  @GetMapping("/internet_things.html")
  public String internet_things(Model model) {
    return "internet_things";
  }

  @GetMapping("/joinus.html")
  public String joinus(Model model) {
    return "joinus";
  }

  @GetMapping("/machine_learning.html")
  public String machine_learning(Model model) {
    return "machine_learning";
  }

  @GetMapping("/partner.html")
  public String partner(Model model) {
    return "partner";
  }

  @GetMapping("/pocket.html")
  public String pocket(Model model) {
    return "pocket";
  }

  @GetMapping("/privacy.html")
  public String privacy(Model model) {
    return "privacy";
  }

  @GetMapping("/product.html")
  public String product(Model model) {
    return "product";
  }

  @GetMapping("/program.html")
  public String program(Model model) {
    return "program";
  }

  @GetMapping("/programming_education.html")
  public String programming_education(Model model) {
    return "programming_education";
  }

  @GetMapping("/school_partner.html")
  public String school_partner(Model model) {
    return "school_partner";
  }

  @GetMapping("/sxt.html")
  public String sxt(Model model) {
    return "sxt";
  }

  @GetMapping("/neutrino.html")
  public String neutrino(Model model) {
    return "neutrino";
  }

  @GetMapping("/aiotookit.html")
  public String aiotookit(Model model) {
    return "aiotookit";
  }

  @GetMapping("/comprehensive.html")
  public String comprehensive(Model model) {
    return "comprehensive";
  }

  @GetMapping("/iotcloud.html")
  public String iotcloud(Model model) {
    return "iotcloud";
  }

  @GetMapping("/ipv6.html")
  public String ipv6(Model model) {
    return "ipv6";
  }

  @GetMapping("/multinetwork.html")
  public String multinetwork(Model model) {
    return "multinetwork";
  }

  @GetMapping("/nbiot.html")
  public String nbiot(Model model) {
    return "nbiot";
  }

  @GetMapping("/rfid.html")
  public String rfid(Model model) {
    return "rfid";
  }

  @GetMapping("/platform.html")
  public String platform(Model model) {
    return "platform";
  }

  @GetMapping("/nestblock.html")
  public String nestblock(Model model) {
    return "nestblock";
  }

  @GetMapping("/blocklypi.html")
  public String blocklypi(Model model) {
    return "blocklypi";
  }
}
