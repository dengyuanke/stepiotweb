package com.stepiot.config;

import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import org.apache.commons.lang3.RandomUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.web.servlet.ServletContextInitializer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class WebConfigurer implements WebMvcConfigurer {

  @Value("${stepiot.static.dns}")
  private String dnstatic;

  @Value("${stepiot.static.deve}")
  private String devestatic;

  @Bean
  public ServletContextInitializer initializer() {
    return new ServletContextInitializer() {
      @Override
      public void onStartup(final ServletContext context) throws ServletException {

        String version = RandomUtils.nextLong() + "";
        context.setAttribute("version", version);

        context.setAttribute("vstatic", "?v=" + version);
        context.setAttribute("dnstatic", dnstatic);
        context.setAttribute("devestatic", devestatic);
      }
    };
  }
}
